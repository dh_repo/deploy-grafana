variable "PROJECT_ID" {
  description = "The ID of the GCP project"
  type        = string
  default = "common-merit-it"
}

variable "REGION" {
  description = "The GCP region to deploy resources in"
  type        = string
  default     = "us-central1"
}

variable "ZONE" {
  description = "The GCP zone to deploy resources in"
  type        = string
  default     = "us-central1-a"
}

variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
  default = "common-merit-net"
}

variable "bucket_name" {
  description = "Name of the storage bucket"
  type        = string
  default = "common-merit-priv-bucket"
}

variable "cluster_name" {
  description = "Name of the Kubernetes cluster"
  type        = string
  default = "common-merit-cluster0"
}

variable "initial_node_count" {
  description = "Initial number of nodes in the Kubernetes cluster"
  type        = number
  default     = 3
}

variable "sql_instance_name" {
  description = "Name of the Cloud SQL instance"
  type        = string
  default = "common-merit-db"
}

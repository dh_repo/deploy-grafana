/**
 * Copyright 2023 common-merit, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

## ---------------------------------------------------------------------------------------------------------------------
# Create a bucket to hold the profile, splash, and cert images
## ---------------------------------------------------------------------------------------------------------------------

resource "google_storage_bucket" "resource_bucket" {
  name          = "sb-profiles-resources"
  location      = var.region
  force_destroy = true

  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD"]
    response_header = ["Content-Type"]
    max_age_seconds = 3600
  }
}

# Create a folder structure in the bucket

resource "google_storage_bucket_object" "image_folder" {
  name    = "img/"
  content = " "
  bucket  = google_storage_bucket.resource_bucket.name
}

resource "google_storage_bucket_object" "profile_pic_folder" {
  name    = "img/profiles/"
  content = " "
  bucket  = google_storage_bucket.resource_bucket.name
}

resource "google_storage_bucket_object" "splash_pic_folder" {
  name    = "img/splash/"
  content = " "
  bucket  = google_storage_bucket.resource_bucket.name
}

resource "google_storage_bucket_object" "badge_pic_folder" {
  name    = "img/badges/"
  content = " "
  bucket  = google_storage_bucket.resource_bucket.name
}

resource "google_storage_bucket_object" "cert_pic_folder" {
  name    = "img/certs/"
  content = " "
  bucket  = google_storage_bucket.resource_bucket.name
}

# grant privileges for users to read from the resource bucket

resource "google_storage_bucket_iam_binding" "legacyObjectReader_role" {
  bucket  = google_storage_bucket.resource_bucket.name
  role    = "roles/storage.legacyObjectReader"
  members = ["allUsers"]
}

## ---------------------------------------------------------------------------------------------------------------------
# Upload some sample profile images to the resource bucket
## ---------------------------------------------------------------------------------------------------------------------

resource "google_storage_bucket_object" "ccook" {
  name   = "img/profiles/1__ChrisCook_59585632.jpg"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/1__ChrisCook_59585632.jpg"
}

resource "google_storage_bucket_object" "jcloud" {
  name   = "img/profiles/2__JoeCloud_99628182.jpg"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/2__JoeCloud_99628182.jpg"
}

resource "google_storage_bucket_object" "bcheese" {
  name   = "img/profiles/3__BigCheese_86917626.jpg"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/3__BigCheese_86917626.jpg"
}

resource "google_storage_bucket_object" "hhoncho" {
  name   = "img/profiles/4__HeadHoncho_11462016.jpg"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/4__HeadHoncho_11462016.jpg"
}

## ---------------------------------------------------------------------------------------------------------------------
# Upload some cert image icons
## ---------------------------------------------------------------------------------------------------------------------

resource "google_storage_bucket_object" "cert-Associate_Cloud_Engineer" {
  name   = "img/certs/Associate_Cloud_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Associate_Cloud_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Cloud_Digital_Leader" {
  name   = "img/certs/Cloud_Digital_Leader.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Cloud_Digital_Leader.png"
}

resource "google_storage_bucket_object" "cert-Google_Cloud_Certified_Fellow" {
  name   = "img/certs/Google_Cloud_Certified_Fellow.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Google_Cloud_Certified_Fellow.png"
}

resource "google_storage_bucket_object" "cert-Looker_Certified_LookML_Developer" {
  name   = "img/certs/Looker_Certified_LookML_Developer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Looker_Certified_LookML_Developer.png"
}

resource "google_storage_bucket_object" "cert-Looker_Certified_Looker_Business_Analyst" {
  name   = "img/certs/Looker_Certified_Looker_Business_Analyst.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Looker_Certified_Looker_Business_Analyst.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_Architect" {
  name   = "img/certs/Professional_Cloud_Architect.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_Architect.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_Database_Engineer" {
  name   = "img/certs/Professional_Cloud_Database_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_Database_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_DevOps_Engineer" {
  name   = "img/certs/Professional_Cloud_DevOps_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_DevOps_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_Developer" {
  name   = "img/certs/Professional_Cloud_Developer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_Developer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_Network_Engineer" {
  name   = "img/certs/Professional_Cloud_Network_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_Network_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Cloud_Security_Engineer" {
  name   = "img/certs/Professional_Cloud_Security_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Cloud_Security_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Data_Engineer" {
  name   = "img/certs/Professional_Data_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Data_Engineer.png"
}

resource "google_storage_bucket_object" "cert-Professional_Google_Workspace_Administrator" {
  name   = "img/certs/Professional_Google_Workspace_Administrator.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Google_Workspace_Administrator.png"
}

resource "google_storage_bucket_object" "cert-Professional_Machine_Learning_Engineer" {
  name   = "img/certs/Professional_Machine_Learning_Engineer.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/Professional_Machine_Learning_Engineer.png"
}

## ---------------------------------------------------------------------------------------------------------------------
# Upload some badge image icons
## ---------------------------------------------------------------------------------------------------------------------

resource "google_storage_bucket_object" "common-merit_Profile_Badge" {
  name   = "img/badges/common-merit-Profile-Badge.png"
  bucket = google_storage_bucket.resource_bucket.name
  source = "images/common-merit-Profile-Badge.png"
}
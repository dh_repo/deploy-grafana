/**
 * Copyright 2023 common-merit, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "instance_name" {
  description = "Name of the MySQL Server"
  type        = string
}

variable "root_password" {
  description = "Password for the MySQL Server root account."
  type        = string
}

variable "custom_cidr_blocks" {
  description = "Custom CIDR blocks to access the database."
  type        = list(any)
}

variable "region" {
  description = "A region is a specific geographical location where you can run your resources."
  type        = string
}

variable "database_name" {
  description = "Name of the MySQL Database"
  type        = string
}

variable "database_user" {
  description = "Name of the MySQL Database User"
  type        = string
}
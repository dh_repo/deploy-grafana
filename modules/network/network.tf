/**
 * Copyright 2023 common-merit, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

## ---------------------------------------------------------------------------------------------------------------------
# Reserve an external IP address
## ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_global_address" "app_ip" {
  name = "profiles-static-ip"
}

## ---------------------------------------------------------------------------------------------------------------------
# Create a Google managed SSL certificate
## ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_managed_ssl_certificate" "lb_default" {
  name = "profiles-ssl-cert"

  managed {
    domains = var.ssl_domain_names
  }
}

## ---------------------------------------------------------------------------------------------------------------------
# Create the load balancer
## ---------------------------------------------------------------------------------------------------------------------

# Create a serverless NEG (network endpoint group) for the App Engine app
resource "google_compute_region_network_endpoint_group" "appengine_neg" {
  name                  = "appengine-neg"
  network_endpoint_type = "SERVERLESS"
  region                = var.region
  app_engine {
    service = "default"
  }
}

# Create a backend service
resource "google_compute_backend_service" "lb_backend" {
  name                  = "lb-backend-service"
  load_balancing_scheme = "EXTERNAL_MANAGED"

  # Add the serverless NEG as a backend to the backend service
  backend {
    group = google_compute_region_network_endpoint_group.appengine_neg.id
  }
}

# Create a URL map to route incoming requests to the backend service
resource "google_compute_url_map" "app_url_map" {
  name            = "app-url-map"
  default_service = google_compute_backend_service.lb_backend.id
}

# Create a target HTTP(S) proxy to route requests to your URL map
resource "google_compute_target_https_proxy" "app_target_proxy" {
  name             = "app-target-proxy"
  ssl_certificates = [google_compute_managed_ssl_certificate.lb_default.id]
  url_map          = google_compute_url_map.app_url_map.id
}

# Create a forwarding rule to route incoming requests to the proxy
resource "google_compute_global_forwarding_rule" "app_fwd_rule" {
  name                  = "app-fwd-rule"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_range            = "443"
  target                = google_compute_target_https_proxy.app_target_proxy.id
  ip_address            = google_compute_global_address.app_ip.address
}
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  project = var.PROJECT_ID
  region  = var.REGION
  zone    = var.ZONE
}

# APIs
module "services" {
  source = "./modules/services"
}

# VPC
resource "google_compute_network" "vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = "true"
}

# Storage Bucket
resource "google_storage_bucket" "bucket" {
  name     = var.bucket_name
  location = var.REGION
}

# Kubernetes Cluster
resource "google_container_cluster" "k8s_cluster" {
  name               = var.cluster_name
  location           = var.ZONE
  initial_node_count = var.initial_node_count
  network = google_compute_network.vpc.name
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.vpc.self_link
}



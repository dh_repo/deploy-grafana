import yaml
import base64


# Load the YAML file
with open('sbe-devops-ssl.yaml', 'r') as file:
    data = yaml.load(file, Loader=yaml.FullLoader)

# Extract the certificate details
certificate = data['data']['tls.crt']
private_key = data['data']['tls.key']

# Decode the base64-encoded certificate and key
certificate = base64.b64decode(certificate).decode('utf-8')
private_key = base64.b64decode(private_key).decode('utf-8')

# Print the certificate and key
print('Certificate:')
print(certificate)
print('Private Key:')
print(private_key)

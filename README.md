1. Deploy the terraform files to your GCP or cloud project. 
2. Deploy Grafana using the CI/CD pipeline by going to build>pipeline>trigger in https://gitlab.com/dh_repo/deploy-grafana/-/pipelines
3. Deploy ArgoCD using the helm chart and the k8s-manifest folder.
4. Verify by navigating to the homepage for both applications. 
from flask import Flask, request, jsonify
import vertexai
from vertexai.language_models import ChatModel, InputOutputTextPair

app = Flask(__name__)

vertexai.init(project="common-merit-it", location="us-central1")
chat_model = ChatModel.from_pretrained("chat-bison@001")
parameters = {
    "candidate_count": 1,
    "max_output_tokens": 256,
    "temperature": 0.2,
    "top_p": 0.8,
    "top_k": 40
}
chat = chat_model.start_chat()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/chat', methods=['POST'])
def get_chat_response():
    user_message = request.json.get('message', '')
    response = chat_model.generate_responses(
        [InputOutputTextPair(user_input=user_message)],
        parameters=parameters
    )[0].response_text
    return jsonify({"response": response})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
